package com.example.clevertask6.di;

import com.example.clevertask6.MapsActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class BuildersModule {

    @ContributesAndroidInjector(modules = MapsActivityModule.class)
    abstract MapsActivity bindActivity();
}
