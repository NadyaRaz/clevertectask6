package com.example.clevertask6.di;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class GsonModule {

    @Provides
    @Singleton
    GsonConverterFactory provideGsonConverterFactoryAll(Gson gson) {
        return GsonConverterFactory.create(gson);
    }

    @Provides
    @Singleton
    Gson provideGson(GsonBuilder builder) {
        return builder.create();
    }

    @Provides
    @Singleton
    GsonBuilder provideGsonBuilder() {
        return new GsonBuilder();
    }
}
