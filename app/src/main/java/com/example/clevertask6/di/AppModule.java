package com.example.clevertask6.di;

import android.content.Context;

import com.example.clevertask6.App;
import com.example.clevertask6.retrofit.BankPointsApiService;
import com.example.clevertask6.retrofit.BankPointsRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module(includes = {ViewModelModule.class})
public class AppModule {

    @Provides
    Context provideContext(App application) {
        return application.getApplicationContext();
    }

    @Singleton
    @Provides
    BankPointsApiService provideBankPointsApiService(Retrofit retrofit) {
        return retrofit.create(BankPointsApiService.class);
    }

    @Provides
    BankPointsRepository provideBankPointsRepository(BankPointsApiService apiService) {
        return new BankPointsRepository(apiService);
    }
}
