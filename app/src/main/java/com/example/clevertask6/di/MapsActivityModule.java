package com.example.clevertask6.di;

import com.example.clevertask6.MainFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MapsActivityModule {

    @ContributesAndroidInjector
    abstract MainFragment contributeMainFragment();
}
