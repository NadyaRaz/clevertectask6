package com.example.clevertask6;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.example.clevertask6.dto.BankPointModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import dagger.android.support.DaggerFragment;

public class MainFragment extends DaggerFragment implements OnMapReadyCallback {

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private final static int ZOOM_LEVEL_CITY = 10;
    private final static int ZOOM_LEVEL_STREET = 15;
    public final static LatLng DEFAULT_COORDINATES = new LatLng(52.425163, 31.015039);
    private final static int LOCATION_PERMISSION_CODE = 1;
    private LatLng currentLocation = null;
    private GoogleMap mMap;
    private BankViewModel viewModel;

    public static MainFragment getInstance(){
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        return inflater.inflate(R.layout.fragment_maps, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = ViewModelProviders.of(requireActivity(), viewModelFactory).get(BankViewModel.class);
        subscribe();
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void subscribe() {
        viewModel.atmsLiveData.observe(this, new Observer<List<BankPointModel>>() {
            @Override
            public void onChanged(List<BankPointModel> bankPointModels) {
                for (BankPointModel atm : bankPointModels) {
                    mMap.addMarker(
                            new MarkerOptions()
                                    .position(new LatLng(Double.parseDouble(atm.getLatitude()), Double.parseDouble(atm.getLongitude())))
                                    .title(atm.getInfo())
                    );
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(DEFAULT_COORDINATES, ZOOM_LEVEL_CITY));
        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(requireActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_CODE);
        } else {
            populateMap();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    populateMap();
                }
                break;
            }
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    @SuppressLint("MissingPermission") //запрашиваем выше
    private void populateMap() {
        mMap.setMyLocationEnabled(true);
        LocationManager locationManager = (LocationManager) requireActivity().getSystemService(Context.LOCATION_SERVICE);
        try {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, ZOOM_LEVEL_STREET));
        } catch (NullPointerException exception) {
            Log.d("CurrentLocationExc", exception.getStackTrace().toString());
        }
        viewModel.loadAtmList("Гомель", currentLocation);
    }
}
