package com.example.clevertask6.dto;

public class BankPointModel {

    private String latitude;
    private String longitude;
    private String info;

    public BankPointModel(String latitude, String longitude, String info) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.info = info;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

}
