package com.example.clevertask6.dto;

import com.google.gson.annotations.SerializedName;

public class BankPointDto {

    @SerializedName("gps_x")
    private String latitude;

    @SerializedName("gps_y")
    private String longitude;

    @SerializedName("work_time")
    private String workTime;

    public BankPointDto(String latitude, String longitude, String workTime) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.workTime = workTime;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getWorkTime() {
        return workTime;
    }

    public void setWorkTime(String workTime) {
        this.workTime = workTime;
    }
}
