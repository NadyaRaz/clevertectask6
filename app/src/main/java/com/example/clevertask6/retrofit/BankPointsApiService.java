package com.example.clevertask6.retrofit;

import com.example.clevertask6.dto.BankPointDto;
import com.example.clevertask6.dto.FilialPointDto;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface BankPointsApiService {

    @GET("atm")
    Single<List<BankPointDto>> getATM(@Query("city") String city);

    @GET("infobox")
    Single<List<BankPointDto>> getInfobox(@Query("city") String city);

    @GET("filials_info")
    Single<List<FilialPointDto>> getFilials(@Query("city") String city);
}
