package com.example.clevertask6;

import android.content.Context;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.clevertask6.dto.BankPointModel;
import com.example.clevertask6.retrofit.BankPointsRepository;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class BankViewModel extends ViewModel {

    public MutableLiveData<List<BankPointModel>> atmsLiveData = new MutableLiveData<>();

    private Context context;
    private BankPointsRepository repository;

    private CompositeDisposable disposables = new CompositeDisposable();

    @Inject
    public BankViewModel(Context context, BankPointsRepository repository) {
        this.context = context;
        this.repository = repository;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposables.clear();
    }

    public void loadAtmList(String city, LatLng currentLocation) {
        disposables.add(
                Single.zip(
                        repository.getAtm(city), repository.getFilials(city), repository.getInfobox(city),
                        (atms, filials, inboxes) -> {
                            ArrayList<BankPointModel> allModels = new ArrayList<>();
                            allModels.addAll(atms);
                            allModels.addAll(filials);
                            allModels.addAll(inboxes);
                            allModels.sort((point1, point2) -> Double.compare(getDistance(point1, currentLocation), getDistance(point2, currentLocation)));
                            return allModels.subList(0, 10);
                        }
                )
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                atms -> {
                                    atmsLiveData.setValue(atms);
                                },
                                error -> {
                                    Toast.makeText(context, "Произошла ошибка во время загрузки данных", Toast.LENGTH_SHORT).show();
                                }
                        )
        );
    }

    private double getDistance(BankPointModel point, LatLng currentLocation) {
        LatLng position;
        if (currentLocation != null) {
            position = currentLocation;
        } else {
            position = MainFragment.DEFAULT_COORDINATES;
        }
        return Math.sqrt(Math.pow(position.latitude - Double.parseDouble(point.getLatitude()), 2) +
                Math.pow(position.longitude - Double.parseDouble(point.getLongitude()), 2));
    }
}
